import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {RouterModule, Routes} from '@angular/router';
import { ListArticleComponent } from './components/list-article/list-article.component';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {ZeniblogService} from './service/zeniblogService';
import {HeaderComponent} from './components/header/header.component';
import {ZeniblogServiceUser} from './service/zeniblogServiceUser';
import {CreateAccountComponent} from './components/create-account/create-account.component';
import { GetAllArticlesComponent } from './components/get-all-articles/get-all-articles.component';
import {WriteArticleComponent} from './components/list-article/write-article/write-article.component';
import {UpdateArticleComponent} from './components/list-article/update-article/update-article.component';
import {ArticleDetailComponent} from './components/list-article/article-detail/article-detail.component';
import {ArticleComponent} from './components/list-article/article/article.component';

const appRoutes: Routes = [
  { path: 'articles', component: ListArticleComponent },
  { path: 'articles/:search', component: ListArticleComponent },
  { path: '',
    redirectTo: 'articles',
    pathMatch: 'full'
  },
  { path: 'getall', component: GetAllArticlesComponent },
  { path: 'write', component: WriteArticleComponent },
  { path: 'create-account', component: CreateAccountComponent },
  { path: 'update/:id', component: UpdateArticleComponent },
  { path: 'article/:id', component: ArticleDetailComponent},
  { path: '**', component: ListArticleComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    ListArticleComponent,
    ArticleComponent,
    ArticleDetailComponent,
    HeaderComponent,
    WriteArticleComponent,
    CreateAccountComponent,
    UpdateArticleComponent,
    GetAllArticlesComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(
      appRoutes
    )
  ],
  providers: [
    ZeniblogService,
    ZeniblogServiceUser
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
