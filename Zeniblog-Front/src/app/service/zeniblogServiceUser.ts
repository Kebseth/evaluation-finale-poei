import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {NewUserModel} from '../model/user.model';

@Injectable()
export class ZeniblogServiceUser {
  constructor(private http: HttpClient) {

  }

  createAccount(user: NewUserModel) {
    return this.http.post(`http://localhost:8080/user`, user);
  }
}
