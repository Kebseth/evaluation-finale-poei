import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ArticleModel} from '../model/article.model';
import {NewArticleModel} from '../model/newArticle.model';

@Injectable()
export class ZeniblogService {
  constructor(private http: HttpClient) {

  }

  getArticles(title: string = null): Observable<ArticleModel[]> {
    let url = '';
    if (title != null) {
      url = `?title=${title}`;
    }
    return this.http.get<ArticleModel[]>(`http://localhost:8080/articles${url}`);
  }

  getAllArticles(): Observable<ArticleModel[]> {
    return this.http.get<ArticleModel[]>('http://localhost:8080/articles/getall');
  }

  getOneArticle(id: string): Observable<ArticleModel> {
    return this.http.get<ArticleModel>(`http://localhost:8080/articles/${id}`);
  }

  createArticle(article: NewArticleModel): Observable<ArticleModel> {
    return this.http.post<ArticleModel>(`http://localhost:8080/articles`, article);
  }

  updateAnArticle(article: NewArticleModel, id: string) {
    return this.http.put(`http://localhost:8080/articles/${id}`, article);
  }

  publishAnArticle(id: string) {
    const body = [];
    return this.http.put(`http://localhost:8080/articles/published/${id}`, body);
  }
}
