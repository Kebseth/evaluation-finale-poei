export class ArticleModel {
  id: string;
  title: string;
  content: string;
  summary: string;
  status: string;
  category: string;
  localDateTime: string;

  constructor(id: string, title: string, content: string, summary: string, status: string, category: string, localDateTime: string) {
    this.id = id;
    this.title = title;
    this.content = content;
    this.summary = summary;
    this.status = status;
    this.category = category;
    this.localDateTime = localDateTime;
  }
}
