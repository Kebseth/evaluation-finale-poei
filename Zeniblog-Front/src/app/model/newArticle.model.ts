export class NewArticleModel {
  title: string;
  content: string;
  summary: string;
  status: string;
  category: string;

  constructor(title: string, content: string, summary: string, category: string) {
    this.title = title;
    this.content = content;
    this.summary = summary;
    this.category = category;
  }
}
