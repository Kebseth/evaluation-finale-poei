import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit {

  @Input() id: string;
  @Input() title: string;
  @Input() summary: string;

  constructor() {
  }

  ngOnInit() {
  }

}
