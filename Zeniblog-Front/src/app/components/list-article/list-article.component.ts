import {Component, OnInit} from '@angular/core';
import {ArticleModel} from '../../model/article.model';
import {ZeniblogService} from '../../service/zeniblogService';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';

@Component({
  selector: 'app-list-article',
  templateUrl: './list-article.component.html',
  styleUrls: ['./list-article.component.css']
})
export class ListArticleComponent implements OnInit {

  listArticles: ArticleModel[];
  search: string;
  filteredArticles: ArticleModel[];
  categories = ['coronavirus', 'jeux', 'sport'];
  category = '';

  constructor(private zeniblogService: ZeniblogService, private router: Router,  private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.search = this.route.snapshot.paramMap.get('search');
    this.getAllArticles();
    this.search = null;
  }

  getAllArticles() {
    if (this.search == null) {
      this.listArticles = null;
      this.zeniblogService.getArticles().subscribe(json => {
          this.listArticles = json;
          this.listArticles.sort((a, b) =>  Date.parse(b.localDateTime) - Date.parse(a.localDateTime));
          this.filteredArticles = this.listArticles;
      });
    } else {
      this.listArticles = null;
      this.zeniblogService.getArticles(this.search).subscribe(json => {
        this.listArticles = json;
        this.listArticles.sort((a, b) =>  Date.parse( b.localDateTime) - Date.parse(a.localDateTime));
        this.filteredArticles = this.listArticles;
      });
    }
  }

  researchOnClick() {
    this.router.navigate([`articles/${this.search}`]);
  }

  onClickFilter() {
    this.filteredArticles = this.listArticles;
    if (this.category !== '') {
      this.filteredArticles = this.filteredArticles.filter(a => a.category === this.category);
    }
  }
}
