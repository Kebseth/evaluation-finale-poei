import {Component, OnInit} from '@angular/core';
import {ZeniblogService} from '../../../service/zeniblogService';
import {ActivatedRoute, Router} from '@angular/router';
import {ArticleModel} from '../../../model/article.model';
import {NewArticleModel} from '../../../model/newArticle.model';

@Component({
  selector: 'app-update-article',
  templateUrl: './update-article.component.html',
  styleUrls: ['./update-article.component.css']
})
export class UpdateArticleComponent implements OnInit {

  id: string;
  article: ArticleModel;
  title = '';
  content = '';
  summary = '';
  newArticle: NewArticleModel;

  constructor(private zeniblogService: ZeniblogService, private route: ActivatedRoute, private router: Router) {
  }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.getUniqArticle();
  }

  getUniqArticle() {
    this.zeniblogService.getOneArticle(this.id).subscribe(json => {
      this.article = json;
      this.title = this.article.title;
      this.content = this.article.content;
      this.summary = this.article.summary;
    });
  }

  updateAnArticle() {
    if (this.article.status === 'DRAFT') {
      if (this.title.length > 0 && this.content.length > 0 && this.summary.length > 0 && this.summary.length < 51) {
        this.newArticle = new NewArticleModel(this.title, this.content, this.summary, this.article.category);
        this.zeniblogService.updateAnArticle(this.newArticle, this.id).subscribe();
        this.router.navigate([`/article/${this.id}`]);
      } else {
        alert('Toute les conditions de sont pas respectés !');
      }
    } else {
      alert('Un Article en DRAFT est inmodifiable');
    }
  }
}
