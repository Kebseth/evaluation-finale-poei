import {Component, OnChanges, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ZeniblogService} from '../../../service/zeniblogService';
import {ArticleModel} from '../../../model/article.model';

@Component({
  selector: 'app-article-detail',
  templateUrl: './article-detail.component.html',
  styleUrls: ['./article-detail.component.css']
})
export class ArticleDetailComponent implements OnInit {

  id: string;
  article: ArticleModel;
  title: string;
  content: string;
  status: string;
  localDateTime: string;
  date: Date;
  dateToDisplay: string;

  constructor(private zeniblogService: ZeniblogService, private route: ActivatedRoute, private router: Router) {
  }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.getUniqArticle();

  }

  getUniqArticle() {
    this.zeniblogService.getOneArticle(this.id).subscribe(json => {
      this.article = json;
      this.status = this.article.status;
      this.title = this.article.title;
      this.content = this.article.content;
      this.localDateTime = this.article.localDateTime;
      this.date = new Date(this.localDateTime);
      this.formatDate();
    });
  }

  publishAnArticle() {
    this.zeniblogService.publishAnArticle(this.id).subscribe();
    this.router.navigate(['/articles']);
  }

  formatDate() {
    this.dateToDisplay = (this.date.toLocaleDateString() + ' à ' + this.date.getHours() + 'h' + this.date.getMinutes()).toString();
  }
}
