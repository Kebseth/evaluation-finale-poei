import {Component, OnInit} from '@angular/core';
import {NewArticleModel} from '../../../model/newArticle.model';
import {ZeniblogService} from '../../../service/zeniblogService';
import {Router} from '@angular/router';

@Component({
  selector: 'app-write-article',
  templateUrl: './write-article.component.html',
  styleUrls: ['./write-article.component.css']
})
export class WriteArticleComponent implements OnInit {

  title = '';
  summary = '';
  content = '';
  category = '';
  article: NewArticleModel;
  categories = ['coronavirus', 'sport', 'jeux'];
  id: string;

  constructor(private zeniblogService: ZeniblogService, private router: Router) {
  }

  ngOnInit() {
  }

  createArticle() {
    if (this.title.length > 0 && this.content.length > 0
      && this.summary.length > 0 && this.summary.length < 51 &&
      this.categories.includes(this.category)) {
      this.article = new NewArticleModel(this.title, this.content, this.summary, this.category);
      this.zeniblogService.createArticle(this.article).subscribe(json => {
        this.id = json.id;
        this.router.navigate([`/article/${this.id}`]);
      });
    } else {
      alert('Toute les conditions de sont pas respectés !');
    }
  }
}
