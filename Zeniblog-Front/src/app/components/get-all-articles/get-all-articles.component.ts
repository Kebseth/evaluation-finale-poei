import { Component, OnInit } from '@angular/core';
import {ArticleModel} from '../../model/article.model';
import {ZeniblogService} from '../../service/zeniblogService';

@Component({
  selector: 'app-get-all-articles',
  templateUrl: './get-all-articles.component.html',
  styleUrls: ['./get-all-articles.component.css']
})
export class GetAllArticlesComponent implements OnInit {

  listArticles: ArticleModel[];

  constructor(private zeniblogService: ZeniblogService) { }

  ngOnInit() {
    this.getAllArticles();
  }

  getAllArticles() {
    this.zeniblogService.getAllArticles().subscribe(json => {
      this.listArticles = json;
    });
  }
}
