import {Component, OnInit} from '@angular/core';
import {NewArticleModel} from '../../model/newArticle.model';
import {NewUserModel} from '../../model/user.model';
import {ZeniblogServiceUser} from '../../service/zeniblogServiceUser';
import {Router} from '@angular/router';

@Component({
  selector: 'app-create-account',
  templateUrl: './create-account.component.html',
  styleUrls: ['./create-account.component.css']
})
export class CreateAccountComponent implements OnInit {

  email = '';
  password = '';
  confirmation = '';
  user: NewUserModel;

  constructor(private zeniblogServiceUser: ZeniblogServiceUser, private router: Router) {
  }

  ngOnInit() {
  }

  createAccount() {
    if (this.email.length > 0 && this.password.length > 0 && this.confirmation.length > 0) {
      if (this.password === this.confirmation) {
        this.user = new NewUserModel(this.email, this.password);
        this.zeniblogServiceUser.createAccount(this.user).subscribe();
        this.router.navigate(['/articles']);
      } else {
        alert('Le mot de passe et sa confirmation sont différents');
      }
    } else {
      alert('Toute les conditions de sont pas respectés !');
    }
  }
}
