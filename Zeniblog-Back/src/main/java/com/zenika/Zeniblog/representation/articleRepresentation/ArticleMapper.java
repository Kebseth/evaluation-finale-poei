package com.zenika.Zeniblog.representation.articleRepresentation;

import com.zenika.Zeniblog.model.Article;
import org.springframework.stereotype.Component;

@Component
public class ArticleMapper {
    public ArticleMapper() {}

    public ArticleRepresentation mapToArticle (Article article) {
        ArticleRepresentation articleToDisplay = new ArticleRepresentation(article.getId(),
                                                    article.getTitle(),
                                                    article.getContent(),
                                                    article.getSummary(),
                                                    article.getStatus(),
                                                    article.getCategory(),
                                                    article.getDate());
        return articleToDisplay;
    }
}
