package com.zenika.Zeniblog.representation.userRepresentation;

import com.zenika.Zeniblog.model.User;
import org.springframework.stereotype.Component;

@Component
public class UserRepresentationMapper {
    public UserRepresentation mapToUser (User user) {
        return new UserRepresentation(user.getId(), user.getEmail(), user.getPassword());
    }
}
