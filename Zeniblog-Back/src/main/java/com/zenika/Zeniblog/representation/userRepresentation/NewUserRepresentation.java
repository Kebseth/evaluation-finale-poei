package com.zenika.Zeniblog.representation.userRepresentation;

import javax.persistence.Id;

public class NewUserRepresentation {

    private String email;
    private String password;

    public NewUserRepresentation(String email, String password) {
        this.email    = email;
        this.password = password;
    }

    public String getEmail() { return email; }
    public String getPassword() { return password; }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
