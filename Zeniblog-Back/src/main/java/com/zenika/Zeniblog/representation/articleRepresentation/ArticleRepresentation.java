package com.zenika.Zeniblog.representation.articleRepresentation;

import com.zenika.Zeniblog.model.Category;
import com.zenika.Zeniblog.model.StatusArticle;
import com.zenika.Zeniblog.model.User;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.time.LocalDateTime;

public class ArticleRepresentation {
    private String id;
    private String title;
    private String content;
    private String summary;
    private StatusArticle status;
    private Category category;
    private LocalDateTime localDateTime;

    protected ArticleRepresentation() {}

    public ArticleRepresentation(String id, String title, String content, String summary, StatusArticle status, Category category, LocalDateTime localDateTime) {
        this.id            = id;
        this.title         = title;
        this.content       = content;
        this.summary       = summary;
        this.status        = status;
        this.category      = category;
        this.localDateTime = localDateTime;
    }

    public String getId() { return id; }
    public String getTitle() { return title; }
    public String getContent() { return content; }
    public String getSummary() { return summary; }
    public StatusArticle getStatus() { return status; }
    public Category getCategory() { return category; }
    public LocalDateTime getLocalDateTime() { return localDateTime; }

    public void setLocalDateTime(LocalDateTime localDateTime) {
        this.localDateTime = localDateTime;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public void setStatus(StatusArticle status) {
        this.status = status;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }
}
