package com.zenika.Zeniblog.representation.userRepresentation;

import javax.persistence.Id;

public class UserRepresentation {

    private String id;
    private String email;
    private String password;

    protected UserRepresentation() {}

    public UserRepresentation(String id, String email, String password) {
        this.id       = id;
        this.email    = email;
        this.password = password;
    }

    public String getEmail() { return email; }
    public String getPassword() { return password; }
    public String getId() { return id; }

    public void setId(String id) {
        this.id = id;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
