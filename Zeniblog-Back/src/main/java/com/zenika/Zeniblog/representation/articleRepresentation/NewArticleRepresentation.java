package com.zenika.Zeniblog.representation.articleRepresentation;

import com.zenika.Zeniblog.model.Category;
import com.zenika.Zeniblog.model.StatusArticle;

public class NewArticleRepresentation {
    private String title;
    private String content;
    private String summary;
    private StatusArticle status;
    private Category category;

    protected NewArticleRepresentation() {}

    public NewArticleRepresentation(String title, String content, String summary, Category category) {
        this.title    = title;
        this.content  = content;
        this.summary  = summary;
        this.status   = StatusArticle.DRAFT;
        this.category = category;
    }

    public String getTitle() { return title; }
    public String getContent() { return content; }
    public String getSummary() { return summary; }
    public Category getCategory() { return category; }

    public void setCategory(Category category) {
        this.category = category;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }
}
