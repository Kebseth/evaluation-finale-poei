package com.zenika.Zeniblog.service;

import com.zenika.Zeniblog.beans.Counter;
import com.zenika.Zeniblog.exception.UserException;
import com.zenika.Zeniblog.model.User;
import com.zenika.Zeniblog.repository.UserRepository;
import com.zenika.Zeniblog.representation.userRepresentation.NewUserRepresentation;
import org.springframework.stereotype.Component;

@Component
public class UserService {
    private Counter counter;
    private UserRepository userRepository;

    public UserService (Counter counter, UserRepository userRepository) {
        this.counter        = counter;
        this.userRepository = userRepository;
    }

    public User createUser(NewUserRepresentation userRepresentation) throws UserException {
        if(userRepository.findByEmail(userRepresentation.getEmail()).isEmpty()) {
            User user = new User(counter.getId(), userRepresentation.getEmail(), userRepresentation.getPassword() );
            return userRepository.save(user);
        } else throw new UserException("Un compte avec ce mail existe déjà");
    }
}
