package com.zenika.Zeniblog.service;

import com.zenika.Zeniblog.beans.Counter;
import com.zenika.Zeniblog.model.Article;
import com.zenika.Zeniblog.model.StatusArticle;
import com.zenika.Zeniblog.repository.ArticleRepository;
import com.zenika.Zeniblog.representation.articleRepresentation.NewArticleRepresentation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.*;

@Component
public class ArticleService {
    private Counter counter;
    private ArticleRepository articleRepository;
    private static final Logger log = LoggerFactory.getLogger(ArticleService.class);

    @Autowired
    public ArticleService(Counter counter, ArticleRepository articleRepository) {
        this.counter           = counter;
        this.articleRepository = articleRepository;
    }

    public List<Article> getAllArticle() {
        return (List<Article>) articleRepository.findAll();
    }

    public List<Article> getAllArticlePublished() {
        return (List<Article>) articleRepository.findAllPublished();
    }

    public Optional<Article> getById(String id) {
        return articleRepository.findById(id);
    }

    @Transactional
    public Optional<Article> createArticle(NewArticleRepresentation article) {
        if (article.getSummary().length() < 51) {
            Article articleToImplement = new Article(counter.getId(),
                    article.getTitle(),
                    article.getContent(),
                    article.getSummary(),
                    article.getCategory(),
                    LocalDateTime.now());
            this.articleRepository.save(articleToImplement);
            return Optional.of(articleToImplement);
        } else throw new RuntimeException("Le résumé doit faire moins de 50 caractères.");
    }

    public Optional<Article> updateAnArticle (String id, NewArticleRepresentation body) {
        Optional<Article> articleToModify = this.articleRepository.findById(id);

        if (articleToModify.isPresent()) {
            //verify title
            if (!body.getTitle().equals("") && body.getTitle() != null) {
                articleToModify.get().setTitle(body.getTitle());
            } else {
                log.info("Le mail reste inchangé");
            }
            //verify summary
            if (!body.getSummary().equals("") && body.getSummary() != null && body.getSummary().length() < 51) {
                articleToModify.get().setSummary(body.getSummary());
            } else {
                log.info("Le resumé reste inchangé");
            }
            //verify content

            if (!body.getContent().equals("") && body.getContent() != null){
                articleToModify.get().setContent(body.getContent());
            }else {
                log.info("Le contenu reste inchangé");
            }
            this.articleRepository.save(articleToModify.get());
        } else {
            throw new IllegalArgumentException("L'ID renseigné n'est pas valide");
        }
        return articleToModify;
    }

    public boolean deleteById(String id) {
        if(this.getById(id).isPresent()) {
            articleRepository.deleteById(id);
            return true;
        }
        else return false;
    }

    public Optional<Article> publishAnArticle (String id) {
        Optional<Article> articleToPublish = this.articleRepository.findById(id);
        if (articleToPublish.isPresent()) {
            articleToPublish.get().setStatus(StatusArticle.PUBLISHED);
            this.articleRepository.save(articleToPublish.get());
        } else {
            throw new IllegalArgumentException("L'ID renseigné n'est pas valide");
        }
        return articleToPublish;
    }

    public List<Article> searchByTitles(String title) {
        return this.articleRepository.searchByTitles(title);
    }

}
