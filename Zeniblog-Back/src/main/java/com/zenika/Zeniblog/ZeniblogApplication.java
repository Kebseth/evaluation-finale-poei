package com.zenika.Zeniblog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZeniblogApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZeniblogApplication.class, args);
	}

}
