package com.zenika.Zeniblog.repository;

import com.zenika.Zeniblog.model.Article;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ArticleRepository extends CrudRepository<Article, String> {

    @Query("SELECT x FROM Article x WHERE x.title LIKE %?1% OR function('levenshtein', x.title, ?1) <= 5 " +
            "AND x.status = 'PUBLISHED'")
    List<Article> searchByTitles(String title);

    @Query("SELECT x FROM Article x WHERE x.status = 'PUBLISHED'")
    List<Article> findAllPublished();
}
