package com.zenika.Zeniblog.model;

public enum  StatusArticle {
    DRAFT,
    PUBLISHED
}
