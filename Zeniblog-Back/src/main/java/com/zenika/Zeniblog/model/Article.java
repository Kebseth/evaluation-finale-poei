package com.zenika.Zeniblog.model;

import org.apache.tomcat.jni.Local;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
@Table(name = "articles")
public class Article {
    @Id
    private String id;
    private String title;
    private String content;
    private String summary;
    @Column(name = "local_date_time", columnDefinition = "TIMESTAMP")
    private LocalDateTime localDateTime;

    @Enumerated(EnumType.STRING)
    private StatusArticle status;

    @Enumerated(EnumType.STRING)
    private Category category;

    @ManyToOne
    @JoinColumn(name = "user_id", updatable = false, insertable = false)
    private User user;

    protected Article() {}

    public Article(String id, String title, String content, String summary, Category category, LocalDateTime localDateTime) {
        this.id       = id;
        this.title    = title;
        this.content  = content;
        this.summary  = summary;
        this.status   = StatusArticle.DRAFT;
        this.category = category;
        this.localDateTime     = localDateTime;
    }

    public String getId() { return id; }
    public String getTitle() { return title; }
    public String getContent() { return content; }
    public String getSummary() { return summary; }
    public StatusArticle getStatus() { return status; }
    public Category getCategory() { return category; }
    public LocalDateTime getDate() { return localDateTime; }

    public void setDate(LocalDateTime localDateTime) {
        this.localDateTime = localDateTime;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public void setStatus(StatusArticle status) {
        this.status = status;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }
}
