package com.zenika.Zeniblog.exception;

public class UserException extends Throwable{
    private String text;

    public UserException(String text) {
        this.text = text;
    }

    public String getText() { return text; }

    public void setText(String text) {
        this.text = text;
    }
}
