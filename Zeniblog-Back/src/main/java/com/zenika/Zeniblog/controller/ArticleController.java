package com.zenika.Zeniblog.controller;

import com.sipios.springsearch.anotation.SearchSpec;
import com.zenika.Zeniblog.beans.Counter;
import com.zenika.Zeniblog.model.Article;
import com.zenika.Zeniblog.repository.ArticleRepository;
import com.zenika.Zeniblog.representation.ResultRepresentation;
import com.zenika.Zeniblog.representation.articleRepresentation.ArticleMapper;
import com.zenika.Zeniblog.representation.articleRepresentation.ArticleRepresentation;
import com.zenika.Zeniblog.representation.articleRepresentation.NewArticleRepresentation;
import com.zenika.Zeniblog.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/articles")
public class ArticleController {

    private ArticleService articleService;
    private Counter counter;
    private ArticleMapper articleMapper;

    @Autowired
    public ArticleController(ArticleService articleService, Counter counter, ArticleMapper articleMapper) {
        this.articleService    = articleService;
        this.counter           = counter;
        this.articleMapper     = articleMapper;
    }

    @GetMapping("/getall")
    public List<ArticleRepresentation> getAll() {
        return this.articleService.getAllArticle().stream()
                .map(article -> articleMapper.mapToArticle(article))
                .collect(Collectors.toList());
    }

    @GetMapping("")
    public List<ArticleRepresentation> getAllPublished(@RequestParam(name = "title", required = false) String title) {
        if(title == null) {
            return this.articleService.getAllArticlePublished().stream()
                    .map(article -> articleMapper.mapToArticle(article))
                    .collect(Collectors.toList());
        }
        else {
            return this.articleService.searchByTitles(title)
                    .stream()
                    .map(article -> articleMapper.mapToArticle(article))
                    .collect(Collectors.toList());
        }
    }


    @GetMapping("/{id}")
    public ResponseEntity<ArticleRepresentation> getById(@PathVariable String id) {
        final Optional<Article> response = this.articleService.getById(id);
        return response.map(recipes -> articleMapper.mapToArticle(recipes))
                .map(ResponseEntity::ok)
                .orElseGet( () -> ResponseEntity.notFound().build());
    }

    @PostMapping
    public ResponseEntity<ArticleRepresentation> createRecipe(@RequestBody NewArticleRepresentation body) {
        return this.articleService.createArticle(body)
                .map(article -> articleMapper.mapToArticle(article))
                .map(ResponseEntity :: ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PutMapping("/{id}")
    public ResponseEntity<ArticleRepresentation> modifyArticle(@PathVariable("id") String id,
                                                              @RequestBody NewArticleRepresentation body) {
        Optional<Article> article = this.articleService.updateAnArticle(id,body);
        return article
                .map(this.articleMapper::mapToArticle)
                .map(ResponseEntity :: ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @DeleteMapping("/{id}")
    public ResultRepresentation deleteById(@PathVariable String id) {
        boolean result = articleService.deleteById(id);
        return new ResultRepresentation(result);
    }

    @PutMapping("/published/{id}")
    public ResponseEntity<ArticleRepresentation> publishAnArticle(@PathVariable("id") String id) {
        Optional<Article> article = this.articleService.publishAnArticle(id);
        return article
                .map(this.articleMapper::mapToArticle)
                .map(ResponseEntity :: ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }
}
