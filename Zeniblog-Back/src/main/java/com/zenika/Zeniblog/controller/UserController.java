package com.zenika.Zeniblog.controller;

import com.zenika.Zeniblog.exception.UserException;
import com.zenika.Zeniblog.representation.userRepresentation.NewUserRepresentation;
import com.zenika.Zeniblog.representation.userRepresentation.UserRepresentation;
import com.zenika.Zeniblog.representation.userRepresentation.UserRepresentationMapper;
import com.zenika.Zeniblog.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/user")
public class UserController {

    private UserService userService;
    private UserRepresentationMapper userRepresentationMapper;

    @Autowired
    public UserController(UserService userService, UserRepresentationMapper userRepresentationMapper) {
        this.userService              = userService;
        this.userRepresentationMapper = userRepresentationMapper;
    }

    @PostMapping
    public UserRepresentation createRecipe(@RequestBody NewUserRepresentation body) {
        try {
            return userRepresentationMapper.mapToUser(userService.createUser(body));
        } catch (UserException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getText());
        }
    }
}
