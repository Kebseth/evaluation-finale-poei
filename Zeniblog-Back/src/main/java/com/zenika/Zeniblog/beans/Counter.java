package com.zenika.Zeniblog.beans;

import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class Counter {
    private String id;

    public String getId() {
        return UUID.randomUUID().toString();
    }

    public Counter myCounter() {
        return new Counter();
    }
}