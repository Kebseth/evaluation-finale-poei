drop table if exists articles;
drop table if exists users ;
CREATE EXTENSION "fuzzystrmatch";

create table users(
    id text primary key,
    email text not null,
    password text not null
);

create table articles(
    id text primary key,
    title text not null,
    content text not null,
    summary text not null,
    user_id text REFERENCES users (id),
    status text not null,
    category text not null,
    local_date_time timestamp not null
);

INSERT INTO users(id, email, password)
values ('1f987a29-dd14-4b19-a9fa-6df4cd4d5caf', 'jean-claude@gmail.fr', 'chaton0404'),
       ('2032def5-4a5a-4bff-951b-a1ea2e83be45', 'alex-du-76@gmail.com', 'gangang'),
       ('9b37b2a1-4f96-4abe-8cc4-7d4ca71ced50', 'marieDupont@cliclic.fr', 'dupont');

INSERT INTO articles(id, title, content, summary, user_id, status, category, local_date_time)
values ('ce469af8-ff35-42de-8b1c-5a734761e100', 'DIRECT. Municipales en Ille-et-ViIaine', 'Comme partout en France, le premier tour des élections municipales en Ille-et-Vilaine a été fortement dégradé par la crise sanitaire du coronavirus. Seulement 44,5 % des électeurs se sont rendus aux urnes contre 64,73 % en 2014, dans un département qui compte plus d’un million d’habitants.
Premier enseignement : Rennes s’ancre solidement à gauche. La maire sortante, la socialiste Nathalie Appéré, est arrivée en tête avec 32 % des suffrages exprimés, devant la liste écologiste menée par l’écologiste Matthieu Theurier avec 25 %. À eux deux, il comptabilise 57 % des voix. C’est mieux qu’en 2014. Sans préjugé du deuxième tour, s’il y en a un, ils gouverneront à nouveau la ville à gauche depuis 1977.

Autre leçon de ce premier tour, la Métropole de Rennes qui accentue son assise à gauche. Deux maires importants de la droite et du centre ont été battus en proche périphérie. Il s’agit du maire UDI de Chantepie, Grégoire Leblond, battu par Gilles Dreuslin qui menait une liste d’union de la gauche. À Bruz, Auguste Louapre a été battu nettement par le divers gauche Philippe Salmon.

À Vitré, l’ancienne députée LR, Isabelle le Callennec, a été élue dès le premier tour avec 55,63 % des suffrages exprimés et et va donc succéder à Pierre Méhaignerie maire depuis 1977.

À Fougères, où il y avait cinq listes, le sortant Louis Feuvrier a été réélu dès le premier tour. Même résultat à Redon où le sortant Pascal Duchène avec 55,25 % des voix.','En Ille-et-Vilaine, 20 points d''absention', '2032def5-4a5a-4bff-951b-a1ea2e83be45', 'PUBLISHED', 'coronavirus', '2020-03-19 09:12:35'),
       ('356d9601-330a-4a7c-8846-5460d7160977', 'Xbox Series X', 'Microsoft a présenté la Xbox Series X, la concurrente de la PS5 sur le marché des consoles next gen. On sait désormais ce à quoi ressemble la console, un peu moins d’un an avant sa sortie. Entre les informations officielles distillées par la firme de Redmond et les fuites dont nous abreuvent les leakers, voici un récapitulatif de tout ce que l’on sait sur la console désignée jusqu’ici sous son nom de code Xbox Scarlett.

. Si après l’incroyable succès commercial de la PS4, la PS5 est particulièrement attendue par les joueurs, Microsoft compte bien reconquérir des parts de marché avec sa Xbox de prochaine génération. Le 12 décembre 2019 à l’occasion des Game Awards, la firme a levé le voile sur la Xbox Series X. C’est donc le nom finalement retenu pour sa prochaine génération de console de salon. Les premières informations sur cette dernière sont déjà disponibles, faisons donc un point sur ce qu’il faut savoir au sujet du « Projet Scarlett ».', 'Xbox Series X : la prochaine console de Microsoft', '1f987a29-dd14-4b19-a9fa-6df4cd4d5caf', 'PUBLISHED', 'jeux', '2020-03-19 09:11:35'),
       ('26336224-517a-46f0-855d-033f85db0134', 'Pogba, Ibrahimovic, Gobert, Chelsea… le monde du sport se mobilise', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce hendrerit nibh a hendrerit commodo. Sed iaculis lacus et euismod consectetur. Aenean pretium pharetra nibh nec vehicula. Sed faucibus molestie odio, vel porta neque gravida eget. Aliquam erat volutpat. Etiam augue nibh, viverra a nulla et, commodo imperdiet est. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nunc interdum iaculis augue eget suscipit. Cras gravida molestie blandit. Nunc nisl urna, facilisis eu odio pulvinar, tempor faucibus felis. Praesent fermentum semper facilisis.

Nulla sit amet mauris sit amet leo semper sollicitudin a eu massa. Curabitur quis urna ut turpis faucibus semper. Vivamus efficitur euismod neque tristique elementum. Mauris fringilla congue ultrices. Morbi vel dictum magna. Morbi eget purus quam. Aliquam arcu magna, molestie sit amet commodo non, placerat vitae sapien. Fusce dolor turpis, posuere ut elementum in, condimentum ut elit. Cras quis justo faucibus nisl convallis tempor sit amet nec neque. Praesent blandit commodo laoreet. Donec elit elit, semper sit amet scelerisque vel, vehicula non quam.',
        'Top 5 des mesures prises par la FIFA', '9b37b2a1-4f96-4abe-8cc4-7d4ca71ced50', 'PUBLISHED', 'sport', '2020-03-19 08:11:35'),
       ('474da573-9452-48c2-8eab-d1d615f641aa', 'Quel est le meilleur itinéraire de footing ?', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce hendrerit nibh a hendrerit commodo. Sed iaculis lacus et euismod consectetur. Aenean pretium pharetra nibh nec vehicula. Sed faucibus molestie odio, vel porta neque gravida eget. Aliquam erat volutpat. Etiam augue nibh, viverra a nulla et, commodo imperdiet est. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nunc interdum iaculis augue eget suscipit. Cras gravida molestie blandit. Nunc nisl urna, facilisis eu odio pulvinar, tempor faucibus felis. Praesent fermentum semper facilisis.

Nulla sit amet mauris sit amet leo semper sollicitudin a eu massa. Curabitur quis urna ut turpis faucibus semper. Vivamus efficitur euismod neque tristique elementum. Mauris fringilla congue ultrices. Morbi vel dictum magna. Morbi eget purus quam. Aliquam arcu magna, molestie sit amet commodo non, placerat vitae sapien. Fusce dolor turpis, posuere ut elementum in, condimentum ut elit. Cras quis justo faucibus nisl convallis tempor sit amet nec neque. Praesent blandit commodo laoreet. Donec elit elit, semper sit amet scelerisque vel, vehicula non quam.',
        'Spoiler alert: Pas chez vous', '9b37b2a1-4f96-4abe-8cc4-7d4ca71ced50', 'PUBLISHED', 'sport', '2020-03-19 08:10:35'),
       ('a48738ee-d8b9-4b49-af34-1599f9001cdf', 'Nouvelle PS5', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce hendrerit nibh a hendrerit commodo. Sed iaculis lacus et euismod consectetur. Aenean pretium pharetra nibh nec vehicula. Sed faucibus molestie odio, vel porta neque gravida eget. Aliquam erat volutpat. Etiam augue nibh, viverra a nulla et, commodo imperdiet est. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nunc interdum iaculis augue eget suscipit. Cras gravida molestie blandit. Nunc nisl urna, facilisis eu odio pulvinar, tempor faucibus felis. Praesent fermentum semper facilisis.

Nulla sit amet mauris sit amet leo semper sollicitudin a eu massa. Curabitur quis urna ut turpis faucibus semper. Vivamus efficitur euismod neque tristique elementum. Mauris fringilla congue ultrices. Morbi vel dictum magna. Morbi eget purus quam. Aliquam arcu magna, molestie sit amet commodo non, placerat vitae sapien. Fusce dolor turpis, posuere ut elementum in, condimentum ut elit. Cras quis justo faucibus nisl convallis tempor sit amet nec neque. Praesent blandit commodo laoreet. Donec elit elit, semper sit amet scelerisque vel, vehicula non quam.',
        'Plus puissante que votre PC, plus rapide que Bolt', '9b37b2a1-4f96-4abe-8cc4-7d4ca71ced50', 'PUBLISHED', 'jeux', '2020-03-19 08:10:34');
